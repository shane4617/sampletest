package testCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import amazon.utils.ORep;
import pages.LaunchPage;

public class TestCase1 {
	@Test
	public void testSteps() {
		WebDriver driver = new ChromeDriver();
		driver.get(ORep.Url);
		LaunchPage lp= PageFactory.initElements(driver, LaunchPage.class);
		lp.enterTextInSearchBox("Video Games");
		lp.clickOnSearchButton();
		lp.clickOnPrimeCheckBox();
		
	}

}
