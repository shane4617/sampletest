package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;

import amazon.utils.ORep;

public class LaunchPage {
	@FindBy(xpath=ORep.LaunchPageSearchBox)
	public WebElement LaunchPageSearchBox;
	
	@FindBy(xpath=ORep.LaunchPageSearchButton)
	public WebElement LaunchPageSearchButton;
	
	@FindBy(linkText=ORep.LaunchPagePrimeChckBox)
	public WebElement LaunchPagePrimeChckBox;
	
	public void enterTextInSearchBox(String ItemYouWantToSearch) {
		LaunchPageSearchBox.sendKeys(ItemYouWantToSearch);
	}
	public void clickOnSearchButton() {
		LaunchPageSearchButton.click();
	}
	public void clickOnPrimeCheckBox() {
		LaunchPagePrimeChckBox.click();
	}
	@SuppressWarnings("unused")
	public void browserSelection(String BrowserName) {
		if(BrowserName.equalsIgnoreCase("Chrome")) {
			WebDriver driver=new ChromeDriver();
		}else if(BrowserName.equalsIgnoreCase("firefox")) {
			WebDriver driver= new FirefoxDriver();
			
		}
		
	}
	
	//sample code for commit changes
	
	public void sample() {
		System.out.println("Sample");
	}
}
